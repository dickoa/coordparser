
# coordparser

<!-- README.md is generated from README.Rmd. Please edit that file -->

[![GitLab CI Build
Status](https://gitlab.com/dickoa/coordparser/badges/master/build.svg)](https://gitlab.com/dickoa/coordparser/pipelines)
[![Codecov Code
Coverage](https://codecov.io/gl/dickoa/coordparser/branch/master/graph/badge.svg)](https://codecov.io/gl/dickoa/coordparser)
[![License:
MIT](https://img.shields.io/badge/License-MIT-yellow.svg)](https://opensource.org/licenses/MIT)

`coordparser` is an R package that parses geographical. It’s a port of
the [coordinate-parser Javascript
library](https://github.com/servant-of-god/coordinate-parser)

## Installation

This package is not on yet on CRAN and to install it, you will need the
[`remotes`](https://github.com/r-lib/remotes) package. You can get
`coordparser` from Gitlab.

``` r
## install.packages("remotes")
remotes::install_gitlab("dickoa/coordparser")
```

## coordparser: A quick tutorial

The main function is `parse_lat_lon`

``` r
library(coordparser)
parse_lat_lon(lat = "40:7:22.8N", lon = "74:7:22.8W")
## latitude longitude
##   40.123   -74.123
```

## Meta

  - Please [report any issues or
    bugs](https://gitlab.com/dickoa/coordparser/issues).
  - License: MIT
  - Please note that this project is released with a [Contributor Code
    of Conduct](CONDUCT.md). By participating in this project you agree
    to abide by its terms.
